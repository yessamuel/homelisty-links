<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://samuelsilva.pt
 * @since      1.0.0
 *
 * @package    Homelisty_Links
 * @subpackage Homelisty_Links/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Homelisty_Links
 * @subpackage Homelisty_Links/includes
 * @author     Samuel Silva <hello@samuelsilva.pt>
 */
class Homelisty_Links {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Homelisty_Links_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'HOMELISTY_LINKS_VERSION' ) ) {
			$this->version = HOMELISTY_LINKS_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'homelisty-links';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
        add_action( 'init', array( $this, 'create_posttypes' ) );

        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'fields-framework/codestar-framework.php';

        $this->create_custom_fields( $this->get_plugin_name() );

        
	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Homelisty_Links_Loader. Orchestrates the hooks of the plugin.
	 * - Homelisty_Links_i18n. Defines internationalization functionality.
	 * - Homelisty_Links_Admin. Defines all hooks for the admin area.
	 * - Homelisty_Links_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-homelisty-links-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-homelisty-links-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-homelisty-links-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-homelisty-links-public.php';

		$this->loader = new Homelisty_Links_Loader();

    }

    private function create_custom_fields( $plugin_name ){
        $plugin_name     = $this->get_plugin_name();
        $homelisty_links = ( get_option('homelisty-links') ? get_option('homelisty-links') : '' );
        $network_fields  = $this->get_network_fields( $homelisty_links['url_fields'] );
        $affiliate_networks = $this->get_networks( $homelisty_links['affiliate_networks'] );
        $advertisers        = $this->get_advertisers();

        CSF::createOptions( $plugin_name, array(
            'menu_title'  => 'Settings',
            'menu_slug'   => 'settings',
            'menu_type'   => 'submenu',
            'menu_parent' => 'edit.php?post_type=homelisty-links',
            'theme'       => 'light',
        ) );

 
        CSF::createSection( $plugin_name, array(
                'title' => __( 'Affiliate Networks', $plugin_name ),
                'fields' => array(
                array(
                    'id'        => 'affiliate_networks',
                    'type'      => 'group',
                    'title'     => __( 'Affiliate Networks', $plugin_name ),
                    'fields'    => array(
                        array(
                            'id'    => 'title',
                            'type'  => 'text',
                            'title' => __( 'Network Name', $plugin_name ),
                        ),
                        // array(
                        //     'id'          => 'url_fields',
                        //     'type'        => 'select',
                        //     'title'       => __('URL Fields', $plugin_name ),
                        //     'subtitle'    => __( 'Fields to construct the URL', $plugin_name ),
                        //     'chosen'      => true,
                        //     'multiple'    => true,
                        //     'placeholder' => __( 'Select fields', $plugin_name ),
                        //     'options'     => $network_fields = $this->get_network_fields( $homelisty_links['url_fields'] ),
                        //   ),
                        array(
                        'id'          => 'url_template',
                        'type'        => 'text',
                        'title'       => __('URL Template', $plugin_name ),
                        'subtitle'    => $this->get_instructions_template( $network_fields ),
                        'chosen'      => true,
                        'multiple'    => true,
                        'placeholder' => __( 'Example: https://url.com/advertiser={advertiserid}', $plugin_name ),
                        ),
                    ),
                ),
            ),
        ) );

        CSF::createSection( $plugin_name, array(
            'title' => __( 'Advertisers', $plugin_name ),
            'fields' => array(
            array(
                'id'        => 'advertisers',
                'type'      => 'group',
                'title'     => __( 'Advertisers', $plugin_name ),
                'fields'    =>  $this->get_new_fields_url( $network_fields, $plugin_name, $affiliate_networks ),
            ),
        ),
    ) );

        CSF::createSection( $plugin_name, array(
            'title' => __( 'Parameters', $plugin_name ),
            'fields' => array(
            array(
                'id'        => 'url_fields',
                'type'      => 'group',
                'title'     => __( 'Insert Parameters', $plugin_name ),
                'subtitle'  => __( '<br><span style="color:green;"><strong>The following auto-populated parameters already exist: </strong> <br><strong>{subid}</strong> = post_id-slug_of_post<br><strong>{subid_s}</strong> = post_idslugofpost<br><strong>{deeplink}</strong> = destination url, <br><strong>{deeplink_decoded}</strong> = destination url (decoded)</span>', $plugin_name ),
                'fields'    => array(
                    array(
                        'id'       => 'field',
                        'type'     => 'text',
                        'title'    => 'Field',
                        'required' => true
                    )
                ),
            ),
        ),
    ) );
   
    CSF::createSection( $plugin_name, array(
        'title' => __( 'Regenerate Links', $plugin_name ),
        'fields' => array(
        array(
            'id'        => 'regenerate_links',
            'type'      => 'select',
            'title'     => __( 'Select Advertiser', $plugin_name ),
            'subtitle'  => '<div id="regenerate-links" class="button button-primary">' . __( 'Regenerate Links', $plugin_name ) . '</div>',
            'options'   => $advertisers,
        ),
        array(
            'id'        => 'regenerate_links_affiliate',
            'type'      => 'select',
            'title'     => __( 'Select Affiliate Network', $plugin_name ),
            'subtitle'  => '<div id="regenerate-links-affiliate" class="button button-primary">' . __( 'Regenerate Links', $plugin_name ) . '</div>',
            'options'   => $affiliate_networks,
        ),
    
    ),
) );





    }
   
    private function get_new_fields_url( $fields, $plugin_name, $networks ){
        $arr = array();
        if( count($fields) ){
            $temp = array(
                'id'    => 'title',
                'type'  => 'text',
                'title' => __( 'Advertiser Name', $plugin_name ),
            );
            array_push( $arr, $temp );
            $temp = array(
                'id'          => 'affiliate_network',
                'type'        => 'select',
                'title'       => __( 'Affiliate Network', $plugin_name ),
                'placeholder' => __( 'Select fields', $plugin_name ),
                'options'     => $networks,
            );
            array_push( $arr, $temp );
            $temp = array(
                'id'     => 'custom_fields',
                'type'   => 'group',
                'title'  => __( 'Parameters', $plugin_name ),
                'fields' => array(),
                'max'    => 1,
                'button_title' => 'Add Parameters',
            );
            array_push( $arr, $temp );

            foreach( $fields as $field ){
                if( $field !== '' ){
                    $temp = array(
                        'id'          => $field,
                        'type'        => 'text',
                        'title'       => $field,
                        'placeholder' => __( 'Leave it blank to not consider.', $plugin_name ),
                    );
                    array_push( $arr[2]['fields'], $temp );
                }
            }
        }
        return $arr;
    }



    private function get_networks( $networks ){
        $options = array();

        if( count($networks) ){
            foreach( $networks as $network ){
                $options[ $network['title'] ] = $network['title'];
            }
        }
        return $options;
    }
    private function get_advertisers(){
        $advertisers = get_option('homelisty-links');
        $advertisers = ( $advertisers ? $advertisers['advertisers'] : false );
        $options = array();
        if( count($advertisers) ){
            foreach( $advertisers as $advertiser ){
                $options[ $advertiser['title'] ] = $advertiser['title'];
            }
        }
        if( ! count($options) ){
            return false;
        }
        return $options;

    }
    private function get_instructions_template( $fields ){
        $instructions = '<strong>Build the tracking template for each affiliate network using the following parameters:</strong> <br>';
        foreach( $fields as $field ){
            $instructions .= '{' . $field . '} ';
        }
        $instructions .= '<br><span style="color:green;"><strong>The following parameters will be auto-populated: </strong> <br>{subid} = post_id-slug_of_post <br>{subid_s} = subid without hyphens <br>{deeplink} = destination url encoded <br>{deeplink_decoded} = destination url decoded<br><br>In case you need more parameters, click on Parameters. </span>';
        return $instructions;
    }
    private function get_network_fields( $fields ){
      
        $arr = array();

        foreach( $fields as $field ){
            $arr[ $field['field'] ] = $field['field'];
        }

        return $arr;
    }
    public function create_posttypes(){
    
        register_post_type( 'homelisty-links',
        // CPT Options
            array(
                'labels' => array(
                    'name'          => __( 'Homelisty Links', $this->get_plugin_name() ),
                    'singular_name' => __( 'Link', $this->get_plugin_name() )
                ),
                'public'              => true,
                'has_archive'         => true,
                'menu_icon'           => 'dashicons-admin-links',
                'exclude_from_search' => true, 
                'rewrite'             => array('slug' => 'go'),
            )
    );
    remove_post_type_support( 'homelisty-links', 'editor' );


    }

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Homelisty_Links_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Homelisty_Links_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Homelisty_Links_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Homelisty_Links_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Homelisty_Links_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
