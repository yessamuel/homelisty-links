var gulp = require('gulp');
var babel = require("gulp-babel");
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

gulp.task('default', () => {
    return gulp.src('admin/js/components/link-editor/index.js')
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['@babel/preset-env']
        }))
        .pipe(concat('editor.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('admin/js'));
});

gulp.task('watch', function () {
    gulp.watch('admin/js/components/link-editor/*.js', gulp.series('default') );
});