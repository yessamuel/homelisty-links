<?php
/**
 * Blocks Initializer
 *
 * Enqueue CSS/JS of all the blocks.
 *
 * @since   1.0.0
 * @package CGB
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Enqueue Gutenberg block assets for both frontend + backend.
 *
 * Assets enqueued:
 * 1. blocks.style.build.css - Frontend + Backend.
 * 2. blocks.build.js - Backend.
 * 3. blocks.editor.build.css - Backend.
 *
 * @uses {wp-blocks} for block type registration & related functions.
 * @uses {wp-element} for WP Element abstraction — structure of blocks.
 * @uses {wp-i18n} to internationalize the block's text.
 * @uses {wp-editor} for WP editor styles.
 * @since 1.0.0
 */
function homelisty_cgb_block_assets() { // phpcs:ignore
	// Register block styles for both frontend + backend.
	wp_register_style(
		'homelisty-cgb-style-css', // Handle.
		plugins_url( 'dist/blocks.style.build.css', dirname( __FILE__ ) ), // Block style CSS.
		is_admin() ? array( 'wp-editor' ) : null, // Dependency to include the CSS after it.
		null // filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.style.build.css' ) // Version: File modification time.
	);

	// Register block editor script for backend.
	wp_register_script(
		'homelisty-cgb-block-js', // Handle.
		plugins_url( '/dist/blocks.build.js', dirname( __FILE__ ) ), // Block.build.js: We register the block here. Built with Webpack.
		array(  'wp-editor' ), // Dependencies, defined above.
		null, // filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.build.js' ), // Version: filemtime — Gets file modification time.
		false // Enqueue the script in the footer.
	);

	// Register block editor styles for backend.
	wp_register_style(
		'homelisty-cgb-block-editor-css', // Handle.
		plugins_url( 'dist/blocks.editor.build.css', dirname( __FILE__ ) ), // Block editor CSS.
		array( 'wp-edit-blocks' ), // Dependency to include the CSS after it.
		null // filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.editor.build.css' ) // Version: File modification time.
	);

	// WP Localized globals. Use dynamic PHP stuff in JavaScript via `cgbGlobal` object.
	wp_localize_script(
		'homelisty-cgb-block-js',
		'homelisty_options', // Array containing dynamic data for a JS Global.
		get_option('homelisty-links')
	);

	/**
	 * Register Gutenberg block on server-side.
	 *
	 * Register the block on server-side to ensure that the block
	 * scripts and styles for both frontend and backend are
	 * enqueued when the editor loads.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/blocks/writing-your-first-block-type#enqueuing-block-scripts
	 * @since 1.16.0
	 */
	register_block_type(
		'cgb/block-homelisty', array(
			// Enqueue blocks.style.build.css on both frontend & backend.
			'style'         => 'homelisty-cgb-style-css',
			// Enqueue blocks.build.js in the editor only.
			'editor_script' => 'homelisty-cgb-block-js',
			// Enqueue blocks.editor.build.css in the editor only.
			'editor_style'  => 'homelisty-cgb-block-editor-css',
		)
	);
}

// Hook: Block assets.
add_action( 'init', 'homelisty_cgb_block_assets' );



// AJAX Stuff

function create_homelisty_link() {

    $url        = (isset($_POST["url"])) ? $_POST["url"] : false;
    $target     = (isset($_POST["target"])) ? $_POST["target"] : false;
    $advertiser = (isset($_POST['advertiser'])) ? $_POST['advertiser'] : false;
    $post_title = (isset($_POST['post_title'])) ? $_POST['post_title'] : false;
    $post_id    = (isset($_POST['post_id'])) ? $_POST['post_id'] : false;
    

    $newlink = array(
        'post_title'    => ($advertiser . ' - ' . $post_id ),
        'post_status'   => 'publish',
        'post_type'     => 'homelisty-links',

      );
       
      // Insert the post into the database
      $newlink_id = wp_insert_post( $newlink );

     update_post_meta( $newlink_id, '_advertiser', $advertiser );
     update_post_meta( $newlink_id, '_homelistylink', $target );
     update_post_meta( $newlink_id, '_finalurl', $url );
    
     
     die( get_the_permalink( $newlink_id ) );

    
    wp_reset_postdata();
}
  
add_action('wp_ajax_nopriv_create_homelisty_link', 'create_homelisty_link');
add_action('wp_ajax_create_homelisty_link', 'create_homelisty_link');



function regenerate_links() {
	$type 		 = (isset($_POST['type']) ? $_POST['type'] : false );
	$advertiser  = (isset($_POST['advertiser']) ? $_POST['advertiser'] : false );
	$affiliate   = (isset($_POST['affiliate']) ? $_POST['affiliate'] : false );
	$affiliate_network = '';
	$affiliate_array = array();
	$output = '<strong>All the links have regenerated!</strong><br><br>';

	$options = get_option('homelisty-links');
	//var_dump($options);

	$advertisers	    = ( $options ? $options['advertisers'] : false );
	$affiliate_networks = ( $options ? $options['affiliate_networks'] : false );
	//var_dump($affiliate_networks);
	//var_dump($advertisers);
	

	if( $type == 'affiliate' ){
		foreach( $advertisers as $temp ){
			if( $temp['affiliate_network'] == $affiliate ){
				array_push($affiliate_array, $temp['title']);
			}
		}
	}
	foreach($advertisers as $temp){
		if($advertiser == $temp['title'] ){
			$advertiser = $temp;
			break;
		}
	}

	

	// get URL template

	$url_template = '';
	foreach( $affiliate_networks as $aff ){
		if( $advertiser['affiliate_network'] == $aff['title'] ){
			$affiliate_network = $aff;
			break;
		}
	}
	$url_template  = $affiliate_network['url_template'];
	$custom_fields = $advertiser['custom_fields'][0];
	
	foreach( $custom_fields as $field ){

	}

	$args = array(
		'post_type'      => 'homelisty-links',
		'posts_per_page' => 500,
		
	);
	
	$posts = new WP_Query($args);

	if( $posts->have_posts() ){

		while( $posts->have_posts() ){

			$posts->the_post();
			$_advertiser = get_post_meta(get_the_ID(), '_advertiser');
			
			if( $type == 'affiliate' ){
				if( ! in_array( $_advertiser[0], $affiliate_array ) ){
					continue;
				}
			} else {
				if( $_advertiser[0] != $advertiser['title'] ){
					continue;
				}
			}
			// get destination_url
			
			$destination_url = get_post_meta( get_the_ID(), '_homelistylink', true);
			$final_old_url = get_post_meta( get_the_ID(), '_finalurl', true);

			//permalink to search posts
			 $permalink = get_the_permalink();

			// get post ID and slug of the post (where link is used)
			 $search_query = get_posts(array('s' => $permalink, 'nopaging' => true));
			 
			 foreach($search_query as $sear ){
				 $post_id = $sear->ID;
				 $slug    = $sear->post_name;	
			 	 break;			 
			 }

			// Now recreate the link

			// replace custom fields

			foreach( $custom_fields as $k=>$field ){
				if( $k == '') {
					continue;
				}
				$url_template = str_replace(  ('{'.$k.'}'), $field, $url_template );
			}
			
			$url_template = str_replace('{deeplink}', urlencode($destination_url), $url_template);
			$url_template = str_replace('{deeplink_decoded}', $destination_url, $url_template);
			$url_template = str_replace('{subid}',  ( $post_id . '-' . $slug ), $url_template);
			$url_template = str_replace('{subid_s}',  ( $post_id . preg_replace('/[^A-Za-z0-9\-]/', '', $slug ) ), $url_template );
			
			$final_new_url = update_post_meta( get_the_ID(), '_finalurl', $url_template);

			$output .= 'Affiliate Network: ' . ( $type=='affiliate' ? $affiliate : $affiliate_network['title'] ) . ' <br>Advertiser: ' . ( $type=='affiliate' ? get_post_meta(get_the_ID(),'_advertiser',true) : $affiliate ) . $advertiser['title'] . '<br>Old URL: ' . $final_old_url . '<br>New URL: ' . $url_template . '<br><br>';

			/*
			url = url.replace( '{deeplink}', encodeURIComponent(target) );
			url = url.replace( '{deeplink_decoded}', target );
			url = url.replace( '{subid}', ( post_id + '-' + post_title ) );
			url = url.replace( '{subid_s}', ( post_id + post_title.replace(/-|\s/g,"") ) );
		   */
			

		}
		die($output);
		wp_reset_postdata();

	}



}


add_action('wp_ajax_nopriv_regenerate_links', 'regenerate_links');
add_action('wp_ajax_regenerate_links', 'regenerate_links');
