/**
 * External dependencies
 */
import classnames from 'classnames';

/**
 * WordPress dependencies
 */
import { __ } from '@wordpress/i18n';
import { Component, Fragment, createRef, useMemo } from '@wordpress/element';
import {
  ExternalLink,
  IconButton,
  ToggleControl,
  SelectControl,
  Button,
  TextControl,
  Notice,
  Spinner,
  withSpokenMessages,
} from '@wordpress/components';
import { LEFT, RIGHT, UP, DOWN, BACKSPACE, ENTER } from '@wordpress/keycodes';
import { getRectangleFromRange } from '@wordpress/dom';
import { prependHTTP, safeDecodeURI, filterURLForDisplay } from '@wordpress/url';
import {
  create,
  insert,
  isCollapsed,
  applyFormat,
  getTextContent,
  slice,
} from '@wordpress/rich-text';
import { URLPopover } from '@wordpress/block-editor';
import apiFetch from '@wordpress/api-fetch';
import { addQueryArgs } from '@wordpress/url';
import URLInput from './url-input';

/**
 * Internal dependencies
 */
import { createLinkFormat, isValidHref } from './utils';

const stopKeyPropagation = ( event ) => event.stopPropagation();

function isShowingInput( props, state ) {
  return props.addingLink || state.editLink;
}

const LinkEditor = ( { value, onChangeInputValue, onKeyDown, submitLink, autocompleteRef } ) => (
  // Disable reason: KeyPress must be suppressed so the block doesn't hide the toolbar
  /* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
  <form
    className="editor-format-toolbar__link-container-content block-editor-format-toolbar__link-container-content"
    onKeyPress={ stopKeyPropagation }
    onKeyDown={ onKeyDown }
    onSubmit={ submitLink }
  >
    <URLInput
      value={ value }
      onChange={ onChangeInputValue }
      autocompleteRef={ autocompleteRef }
    />
    <IconButton icon="editor-break" label={ __( 'Insert Homelisty Link' ) } type="submit" />
  </form>
  /* eslint-enable jsx-a11y/no-noninteractive-element-interactions */
);

const LinkViewerUrl = ( { url } ) => {
  const prependedURL = prependHTTP( url );
  const linkClassName = classnames( 'editor-format-toolbar__link-container-value block-editor-format-toolbar__link-container-value', {
    'has-invalid-link': ! isValidHref( prependedURL ),
  } );

  if ( ! url ) {
    return <span className={ linkClassName }></span>;
  }

  return (
    <ExternalLink
      className={ linkClassName }
      href={ url }
    >
      { filterURLForDisplay( safeDecodeURI( url ) ) }
    </ExternalLink>
  );
};

const URLPopoverAtLink = ( { isActive, addingLink, value, ...props } ) => {
  const anchorRect = useMemo( () => {
    const selection = window.getSelection();
    const range = selection.rangeCount > 0 ? selection.getRangeAt( 0 ) : null;
    if ( ! range ) {
      return;
    }

    if ( addingLink ) {
      return getRectangleFromRange( range );
    }

    let element = range.startContainer;

    // If the caret is right before the element, select the next element.
    element = element.nextElementSibling || element;

    while ( element.nodeType !== window.Node.ELEMENT_NODE ) {
      element = element.parentNode;
    }

    const closest = element.closest( 'a' );
    if ( closest ) {
      return closest.getBoundingClientRect();
    }
  }, [ isActive, addingLink, value.start, value.end ] );

  if ( ! anchorRect ) {
    return null;
  }

  return <URLPopover anchorRect={ anchorRect } { ...props } />;
};

const LinkViewer = ( { url, editLink } ) => {
  return (
    // Disable reason: KeyPress must be suppressed so the block doesn't hide the toolbar
    /* eslint-disable jsx-a11y/no-static-element-interactions */
    <div
      className="editor-format-toolbar__link-container-content block-editor-format-toolbar__link-container-content"
      onKeyPress={ stopKeyPropagation }
    >
      <LinkViewerUrl url={ url } />
      <IconButton icon="edit" label={ __( 'Edit' ) } onClick={ editLink } />
    </div>
    /* eslint-enable jsx-a11y/no-static-element-interactions */
  );
};


const createNewHomelistyLink = (target, advertiser, noFollow) => {
    const $ = jQuery;
    const post_id = $('#post_ID').val();
    const post_title = wp.data.select("core/editor").getPermalinkParts().postName;
    const url_template = '';
    console.log(post_id);
    const advertiser_obj = homelisty_options.advertisers.find( adv => adv.title === advertiser );
    const network = homelisty_options.affiliate_networks.find( net => net.title === advertiser_obj.affiliate_network );
    let url = network.url_template;


    console.log(advertiser_obj);
    console.log(network);

    let custom_fields = advertiser_obj['custom_fields'][0];

    if( typeof custom_fields == 'undefined'  ){
        return;
    }
    for (var key in custom_fields) { 
        console.log( key );
        if( custom_fields[key] == '' ){
            continue;
        }
        url = url.replace( '{'+key+'}',custom_fields[key]  );
    }

    url = url.replace( '{deeplink}', encodeURIComponent(target) );
    url = url.replace( '{deeplink_decoded}', target );
    url = url.replace( '{subid}', ( post_id + '-' + post_title ) );
    url = url.replace( '{subid_s}', ( post_id + post_title.replace(/-|\s/g,"") ) );
    console.log(url);

  return new Promise((resolve, reject) => {

    
    

    jQuery.post(
      ajaxurl,
      {
        action: 'create_homelisty_link',
        url: url,
        target: target,
        advertiser: advertiser,
        post_title: advertiser + '-' + post_id,
        post_id: post_id,
        redirect: '',
        nofollow: noFollow,
        tracking: 1,
        sponsored: 0
      },
      (data, textStatus, xhr) => {
            console.log(data);
            resolve(data);
      }
      ).fail(error => {
        console.log(error);
        reject(error);
      });
  });
}

class InlineLinkUI extends Component {
  constructor() {
    super( ...arguments );

    this.editLink = this.editLink.bind( this );
    this.submitLink = this.submitLink.bind( this );
    this.onKeyDown = this.onKeyDown.bind( this );
    this.onChangeInputValue = this.onChangeInputValue.bind( this );
    this.setNoFollow = this.setNoFollow.bind( this );
    this.setLinkTarget = this.setLinkTarget.bind( this );
    this.onClickOutside = this.onClickOutside.bind( this );
    this.resetState = this.resetState.bind( this );
    this.autocompleteRef = createRef();

    this.state = {
      noFollow: true,
      opensInNewWindow: true,
      advertiser: '',
      inputValue: '',
      newLinkUrl: '',
      creatingLink: false,
      createdLink: false,
      createdLinkError: false
    };
  }

  static getDerivedStateFromProps( props, state ) {
    const { activeAttributes: { url, target } } = props;
    const opensInNewWindow = true;

    if ( ! isShowingInput( props, state ) ) {
      if ( url !== state.inputValue ) {
        return { inputValue: url };
      }

      if ( opensInNewWindow !== state.opensInNewWindow ) {
        return { opensInNewWindow };
      }
    }

    return null;
  }

  onKeyDown( event ) {
    if ( [ LEFT, DOWN, RIGHT, UP, BACKSPACE, ENTER ].indexOf( event.keyCode ) > -1 ) {
      // Stop the key event from propagating up to ObserveTyping.startTypingInTextField.
      event.stopPropagation();
    }
  }

  onChangeInputValue( inputValue ) {
    this.setState( { inputValue } );
  }

  setLinkTarget( opensInNewWindow ) {
    const { activeAttributes: { url = '' }, value, onChange } = this.props;

    this.setState( { opensInNewWindow } );

    // Apply now if URL is not being edited.
    if ( ! isShowingInput( this.props, this.state ) ) {
      const selectedText = getTextContent( slice( value ) );

      onChange( applyFormat( value, createLinkFormat( {
        url,
        opensInNewWindow,
        text: selectedText,
      } ) ) );
    }
  }

  setNoFollow( noFollow ) {
    const { activeAttributes: { url = '' }, value, onChange } = this.props;

    this.setState( { noFollow } );

    // Apply now if URL is not being edited.
    if ( ! isShowingInput( this.props, this.state ) ) {
      const selectedText = getTextContent( slice( value ) );

      onChange( applyFormat( value, createLinkFormat( {
        url,
        opensInNewWindow,
        text: selectedText,
        noFollow,
        advertiser
      } ) ) );
    }
  }

  editLink( event ) {
   // this.setState( { editLink: true } );
   // event.preventDefault();
  }

  submitLink( event ) {
    const { isActive, value, onChange, speak } = this.props;
    const { inputValue, opensInNewWindow, noFollow, advertiser } = this.state;
    const url = prependHTTP( inputValue );
    const selectedText = getTextContent( slice( value ) );
    const format = createLinkFormat( {
      url,
      opensInNewWindow,
      text: selectedText,
      noFollow,
      advertiser
    } );

    //event.preventDefault();

    if ( isCollapsed( value ) && ! isActive ) {
      const toInsert = applyFormat( create( { text: url } ), format, 0, url.length );
      onChange( insert( value, toInsert ) );
    } else {
      onChange( applyFormat( value, format ) );
    }

    this.resetState();

    if ( ! isValidHref( url ) ) {
      speak( __( 'Warning: the link has been inserted but may have errors. Please test it.' ), 'assertive' );
    } else if ( isActive ) {
      speak( __( 'Link edited.' ), 'assertive' );
    } else {
      speak( __( 'Link inserted.' ), 'assertive' );
    }
  }

  onClickOutside( event ) {
    // The autocomplete suggestions list renders in a separate popover (in a portal),
    // so onClickOutside fails to detect that a click on a suggestion occurred in the
    // LinkContainer. Detect clicks on autocomplete suggestions using a ref here, and
    // return to avoid the popover being closed.
    const autocompleteElement = this.autocompleteRef.current;
    if ( autocompleteElement && autocompleteElement.contains( event.target ) ) {
      return;
    }

    this.resetState();
  }

  resetState() {
    this.props.stopAddingLink();
    this.setState( { editLink: false } );
  }

  advertisers() {
        let advertisers_options = [{label: 'Select an option', value: 'Select an option'}];
        let advertisers = homelisty_options.advertisers;
        for( var i=0; i<advertisers.length; i++ ){
            if(advertisers[i]['affiliate_network'] !== '' ){
                advertisers_options.push(  { label: advertisers[i]['title'], value: advertisers[i]['title'] } );
            }
        }
    
        return advertisers_options;
  }
  associateLink(url){
      setTimeout(function(){
        $('[aria-label="Insert Homelisty Link"]').trigger('click');
      },300);
  }
  render() {
    const { isActive, activeAttributes: { url }, addingLink, value } = this.props;

    if ( ! isActive && ! addingLink ) {
      return null;
    }

    const { inputValue, noFollow, opensInNewWindow, advertiser, newLinkUrl, creatingLink, createdLink, createdLinkError } = this.state;
    const showInput = isShowingInput( this.props, this.state );
    return (
      <URLPopoverAtLink
        className="homelisty-link-inserter"
        value={ value }
        isActive={ isActive }
        addingLink={ addingLink }
        onClickOutside={ this.onClickOutside }
        onClose={ this.resetState }
        focusOnMount={ showInput ? 'firstElement' : false }
        renderSettings={ () => (
          <Fragment>
            <div>
              <ToggleControl
                label={ __( 'Open in New Tab' ) }
                checked={ opensInNewWindow }
                onChange={ this.setLinkTarget }
              />
              <ToggleControl
                label={ __( 'Nofollow' ) }
                checked={ noFollow }
                onChange={ this.setNoFollow }
              />
              
              <SelectControl
                    label={ __( 'Select Advertiser' ) }
                    value={ advertiser }
                    options={ this.advertisers() }
                    onChange={ ( advertiser ) => { this.setState( { advertiser } ) } }
                />
            </div>
            <div className="homelisty-link-inserter-form-container">
              {
                createdLink && (
                    
                  <Notice status="success" onRemove={() => this.setState({createdLink: false})}>
                        <p>{__( 'Homelisty Link created successfully.', 'memberpress' )}</p>
                    </Notice>
                )
              }
              {
                createdLinkError && (
                  <Notice status="error" onRemove={() => this.setState({createdLink: false, createdLinkError: false})}>
                        <p>{__( 'Homelisty Link could not be created. Please try a slug that is not already used.', 'memberpress' )}</p>
                    </Notice>
                )
              }
              <strong>{__('New Homelisty Link', 'homelisty-link')}</strong>
              <form onSubmit={(event) => {
                console.log('Avertiser: ' + advertiser);
                event.preventDefault();
                // Send request to create new Homelisty Link
                this.setState({
                  creatingLink: true,
                  createdLinkError: false,
                });
                createNewHomelistyLink( newLinkUrl, advertiser )
                  .then(data => {
                    console.log('then: ' + data);
                    this.associateLink(data);
                    this.setState({
                      createdLink: true,
                      creatingLink: false,
                      inputValue: data,
                      newLinkUrl: '',
                    });
                  })
                  .catch(error => {
                    this.setState({
                      createdLink: false,
                      creatingLink: false,
                      createdLinkError: true,
                    });
                  });
              }}>
                <p>
                  <TextControl
                        placeholder="URL"
                        className="homelisty-link-new-link-url"
                        value={newLinkUrl}
                        onChange={ ( newLinkUrl ) => {
                          this.setState( { newLinkUrl } );
                        } }
                    />
                </p>
                  <p>
                    <button
                      className="homelisty-link-submit-new-link components-button is-button is-primary"
                      onClick={ () => {
                        console.log('Creating new Homelisty Link...');
                      } }
                    >
                      { __( 'Create New Homelisty Link', 'homelisty-link' ) }
                    </button>
                    {
                      creatingLink && (
                        <Spinner />
                      )
                    }
                  </p>
              </form>
            </div>
          </Fragment>
        ) }
      >
          <LinkEditor
            value={ inputValue }
            onChangeInputValue={ this.onChangeInputValue }
            onKeyDown={ this.onKeyDown }
            submitLink={ this.submitLink }
            autocompleteRef={ this.autocompleteRef }
          />
  
      </URLPopoverAtLink>
    );
  }
}

export default withSpokenMessages( InlineLinkUI );
