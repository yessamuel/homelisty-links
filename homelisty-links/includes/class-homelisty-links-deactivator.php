<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://samuelsilva.pt
 * @since      1.0.0
 *
 * @package    Homelisty_Links
 * @subpackage Homelisty_Links/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Homelisty_Links
 * @subpackage Homelisty_Links/includes
 * @author     Samuel Silva <hello@samuelsilva.pt>
 */
class Homelisty_Links_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
