// use at backoffice homelisty post types. Disabled by now

(function ($) {
    'use strict';


})(jQuery);
var $ = jQuery;

$(document).ready(function () {
    hide_buttons();


    $('body').on('click', '[data-field-id="[advertisers]"] > .csf-cloneable-item .csf-field-group .button, [data-field-id="[advertisers]"] > .csf-cloneable-item .csf-cloneable-title, [data-field-id="[advertisers]"] > .csf-cloneable-item .csf-cloneable-remove', function () {
        hide_buttons();
    });




});

var hide_buttons = function () {
    var i = 0;
    $('[data-field-id="[advertisers]"] > .csf-cloneable-item .csf-field-group').each(function () {
        i++;
        var _this = $(this);
        console.log(_this);
        console.log(_this.find('.csf-cloneable-title').is(':visible'));

        if (_this.find('.csf-cloneable-title').is(':visible')) {
            _this.find('.button').css('display', 'none');
        } else {
            _this.find('.button').css('display', 'inline-block');

        }
    });

}

$(document).ready(function () {



    $('body').on('click', '#regenerate-links, #regenerate-links-affiliate', function () {
        var _this = $(this);
        var type = 'advertiser';
        if( _this.attr('id') == 'regenerate-links-affiliate' ){
                type = 'affiliate';
        }
        var advertiser = $('[name="homelisty-links[regenerate_links]"]').val();
        var affiliate = $('[name="homelisty-links[regenerate_links_affiliate]"]').val();
        
        if (advertiser) {
            _this.after('<p>Wait a moment...</p>');
            _this.attr('disabled','disabled')

            $.ajax({
                url: ajaxurl,
                type: 'post',
                data: {
                    action: 'regenerate_links',
                    advertiser: advertiser,
                    affiliate: affiliate,
                    type: type
                },
                dataType: 'html',
                success: function (result) {
                    console.log('success');
                    console.log(result);
                    _this.removeAttr('disabled')
                    _this.after('<p>' + result + '</p>');
                },
                error: function (error) {
                    console.log(error);
                    _this.removeAttr('disabled')
                    alert('Error. View JS console to view the errors.');
                }
            });




        }

    });


});