<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://samuelsilva.pt
 * @since      1.0.0
 *
 * @package    Homelisty_Links
 * @subpackage Homelisty_Links/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Homelisty_Links
 * @subpackage Homelisty_Links/admin
 * @author     Samuel Silva <hello@samuelsilva.pt>
 */
class Homelisty_Links_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
        add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );
        add_action( 'save_post', array( $this, 'save_homelisty_link' ) );
        add_action( 'save_post', array( $this, 'save_finalurl' ) );
        add_action( 'save_post', array( $this, 'save_advertiser' ) );

	}
    function add_meta_box() {
        $screens = array( 'homelisty-links' );
        foreach ( $screens as $screen ) {
            add_meta_box(
            'homelistylink',
            'homelistylink',
            array( $this, 'show_custom_meta_box' ),
            $screen,
            'normal',
            'high'
            );
        }
    }

    function show_custom_meta_box( $post ) {
        wp_nonce_field( 'homelistylink', 'homelistylink_nonce' );
        wp_nonce_field( 'finalurl', 'finalurl_nonce' );
        wp_nonce_field( 'advertiser', 'advertiser_nonce' );
        
        $homelisty_link = get_post_meta( $post->ID, '_homelistylink', true );
        $finalurl_link  = get_post_meta( $post->ID, '_finalurl', true );
        $advertiser     = get_post_meta( $post->ID, '_advertiser', true );

        $advertisers    = $this->get_advertisers();
        $url_template   = $this->get_url_template( $advertiser );
        echo '<label for="homelistylink">';
        _e( 'Select Advertiser', $this->plugin_name );
        echo '</label> ';
        echo '<select id="advertiser" name="advertiser">';
        foreach( $advertisers as $adv ){
            $selected = '';
            if( $advertiser == $adv['title'] ){
                $selected = 'selected';
            }
            echo '<option data-urltemplate="' . $url_template . '" value="' . $adv['title'] . '"' .  $selected . '>' . $adv['title'] . '</option>'; 
        }
        echo '</select>';

        echo '<label for="homelistylink">';
        _e( 'Destination URL', $this->plugin_name );
        echo '</label> ';
        echo '<input type="text" id="homelisty_link" name="homelistylink" value="' . esc_attr( $homelisty_link ) . '"  />';

        echo '<label for="finalurl">';
        _e( 'Final URL', $this->plugin_name );
        echo '</label> ';
        echo '<input type="text" id="finalurl" name="finalurl" value="' . esc_attr( $finalurl_link ) . '"  />';
    }

    function save_homelisty_link( $post_id ) {
        
        if ( ! isset( $_POST['homelistylink_nonce'] ) ) {
            return;
        }
        if ( ! wp_verify_nonce( $_POST['homelistylink_nonce'], 'homelistylink' ) ) {
            return;
        }
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
            if ( ! current_user_can( 'edit_page', $post_id ) ) {
                return;
            }
        } else {
            if ( ! current_user_can( 'edit_post', $post_id ) ) {
                return;
            }
        }
        if ( ! isset( $_POST['homelistylink'] ) ) {
            return;
        }
        $my_data = sanitize_text_field( $_POST['homelistylink'] );

        update_post_meta( $post_id, '_homelistylink', $my_data );

      
    }



    function save_finalurl( $post_id ) {
        if ( ! isset( $_POST['finalurl_nonce'] ) ) {
            return;
        }
        if ( ! wp_verify_nonce( $_POST['finalurl_nonce'], 'finalurl' ) ) {
            return;
        }
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
            if ( ! current_user_can( 'edit_page', $post_id ) ) {
                return;
            }
        } else {
            if ( ! current_user_can( 'edit_post', $post_id ) ) {
                return;
            }
        }
        if ( ! isset( $_POST['finalurl'] ) ) {
            return;
        }
        $my_data = sanitize_text_field( $_POST['finalurl'] );
        update_post_meta( $post_id, '_finalurl', $my_data );
    }



    function save_advertiser( $post_id ) {
        if ( ! isset( $_POST['homelistylink_nonce'] ) ) {
            return;
        }
        if ( ! wp_verify_nonce( $_POST['homelistylink_nonce'], 'homelistylink' ) ) {
            return;
        }
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
            if ( ! current_user_can( 'edit_page', $post_id ) ) {
                return;
            }
        } else {
            if ( ! current_user_can( 'edit_post', $post_id ) ) {
                return;
            }
        }
        if ( ! isset( $_POST['advertiser'] ) ) {
            return;
        }
        $my_data = sanitize_text_field( $_POST['advertiser'] );
        update_post_meta( $post_id, '_advertiser', $my_data );
    }
    

    private function get_url_template( $affiliate = false ){
        if( ! $affiliate ){
            return;
        }
        $data = get_option('homelisty-links');
        $key = array_search( $affiliate, array_column($data['affiliate_networks'], 'title'));

        return $data['affiliate_networks'][$key]['url_template'];

    }
    private function get_advertisers(){
        $advertisers = get_option('homelisty-links');
        $advertisers = ( $advertisers ? $advertisers['advertisers'] : false );
        return $advertisers;
    }
	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/homelisty-links-admin.css', array(), $this->version, 'all' );

    }
    


	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/homelisty-links-admin.js', array( 'jquery' ), $this->version, false );



	}

}
