<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://samuelsilva.pt
 * @since      1.0.0
 *
 * @package    Homelisty_Links
 * @subpackage Homelisty_Links/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
