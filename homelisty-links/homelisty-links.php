<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://samuelsilva.pt
 * @since             1.0.1
 * @package           Homelisty_Links
 *
 * @wordpress-plugin
 * Plugin Name:       Homelisty Links
 * Plugin URI:        homelisty-links
 * Description:       This plugin provides a better optimized way to create affiliates links for Homelisty website.
 * Version:           1.1.3
 * Author:            Samuel Silva
 * Author URI:        https://samuelsilva.pt
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       homelisty-links
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'HOMELISTY_LINKS_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-homelisty-links-activator.php
 */
function activate_homelisty_links() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-homelisty-links-activator.php';
	Homelisty_Links_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-homelisty-links-deactivator.php
 */
function deactivate_homelisty_links() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-homelisty-links-deactivator.php';
	Homelisty_Links_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_homelisty_links' );
register_deactivation_hook( __FILE__, 'deactivate_homelisty_links' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-homelisty-links.php';
require plugin_dir_path( __FILE__ ) . 'admin/editor/plugin.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_homelisty_links() {

	$plugin = new Homelisty_Links();
	$plugin->run();

}
run_homelisty_links();
